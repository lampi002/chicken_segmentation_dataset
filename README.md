# chicken_segmentation_dataset

This repo includes rgb-images of chickens, box and mask annotations for each image. All images are padded and resized to a resolution of 384x384 pixels. 
